<?php
require_once "GeoProcessor.inc";

class Order  {
       
    public function Order(){
    }
        
    /**
     * This function processes an order.
     * @param $orderList.  An array of products in the order which includes name and quantity.
     * @param $shipToAddress.  The location to ship the order
     * @param $warehouse. Information about warehouse and its inventory
     * @return array.  Array with status and message for creating the warehouse
     */
    public static function processOrder($orderList, $shipToAddress, $warehouse){
        $result = array("status" => "OK", "message" => "");

        // Get coordinates for ship to address
        $coordinates = GeoProcessor::getCoordinates($shipToAddress);
        if (is_null($coordinates)) {
            $result["status"] = "WARNING";
            $result["message"] .= "Cannot get coordinates for $shipToAddress; ";
            return $result;
        }
        $latitude = $coordinates["latitude"];
        $longitude = $coordinates["longitude"];
        
        // Get the coordinates for each warehouse and find the distance from the warehouse to ship to address.
        $warehouseCoordinateList = $warehouse->getWarehouseListAndCoordinates();
        $shippingDistanceFromWarehouseList = array();
        foreach ($warehouseCoordinateList as $aWarehouse)  {
            $whName = $aWarehouse["name"];    
            $whLatitude = $aWarehouse["latitude"];    
            $whLongitude = $aWarehouse["longitude"];    
            
            $distance = GeoProcessor::getDistanceBetweenCoordinates($whLatitude, $whLongitude, $latitude, $longitude);
            $shippingDistanceFromWarehouseList[$whName] = $distance;
        }
        
        // Sort the warehouse list in ascending order of distance.  Find which warehouse can fulfill the order.
        asort($shippingDistanceFromWarehouseList);
        $warehouseToShipFrom = "";
        foreach ($shippingDistanceFromWarehouseList as $whName => $distance)  {
            if ($warehouse->doesWarehouseFulfillOrder($whName, $orderList)) {
                $warehouseToShipFrom = $whName;
                break;
            }
        }
        if (strlen($warehouseToShipFrom)==0) {
            $result["status"] = "WARNING";
            $result["message"] = "Your order is on backlog";
            return $result;
        }
        
        // Update the warehouse inventory after the order is made.
        $warehouse->updateInventoryAfterOrder($warehouseToShipFrom, $orderList);
        $result["message"] = "Your order will be shipped from $warehouseToShipFrom";
        return $result;
    }
}
        
?>