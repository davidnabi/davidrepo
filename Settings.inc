<?php
class Settings {
    public static $DB_HOST = '******';   // @@@@ SET DB_HOST
    public static $DB_USER = '******';   // @@@@ SET DB_USER
    public static $DB_PW = '******';     // @@@@ SET DB_PW
    public static $DB_NAME = 'Shipwire';
    public static $TABLE_WH = 'Warehouse';
    public static $TABLE_WH_INV = 'WarehouseInventory';
    public static $TABLE_PROD = 'Product';
    
    public static $GOOGLE_API_KEY = '********************';  // @@@@ SET KEY  
}    
?>