<?php
require_once "Settings.inc";

class DBHelper  {    
     private $dbLink;
     private $server;
     private $user;
     private $password;
     private $dbName;

     
    /**
     * Constructor.  Initializes MySQL database access information.  Internal settings are used from
     *   Settings class.
     */
     public function DBHelper(){
         $this->server = Settings::$DB_HOST;
         $this->user = Settings::$DB_USER;
         $this->password = Settings::$DB_PW;
         $this->dbName = Settings::$DB_NAME;
     }
     
    /**
     * This function creates the Shipwire database in MySQL and the tables to manage Merchants inventory.
     * @return boolean.  True is returned if database and tables can be created.
     */
    private function initializeDb()  {
        $sql = "CREATE DATABASE " . $this->dbName;
        $result = mysql_query($sql, $this->dbLink);
        if (! $result)
            return false;
        $result = mysql_select_db($this->dbName, $this->dbLink);
        if (! $result)
            return false;
        $sql = 'CREATE  TABLE ' . Settings::$TABLE_WH .
                '(`id` INT NOT NULL AUTO_INCREMENT ,
                 `name` VARCHAR(255) NOT NULL ,
                 `address` VARCHAR(255) NOT NULL ,
                 `longitude` DOUBLE NOT NULL ,
                 `latitude` DOUBLE NOT NULL ,
                 PRIMARY KEY (`id`) )';
        $result = mysql_query($sql, $this->dbLink);
        if (! $result)
            return false;
        $sql = 'CREATE  TABLE ' . Settings::$TABLE_WH_INV . 
                '(`warehouse` VARCHAR(255) NOT NULL ,
                 `product` VARCHAR(255) NOT NULL ,
                 `quantity` INT NOT NULL  )';
        $result = mysql_query($sql, $this->dbLink);
        if (! $result)
            return false;
        $sql = 'CREATE  TABLE ' . Settings::$TABLE_PROD . 
                ' (`id` INT NOT NULL AUTO_INCREMENT ,
                 `name` VARCHAR(255) NOT NULL ,
                 `dimensions` VARCHAR(255) NOT NULL ,
                 `weight` VARCHAR(255) NOT NULL, 
                 PRIMARY KEY (`id`) )';
        $result = mysql_query($sql, $this->dbLink);
        if (! $result)
            return false;
        return true;
     }
     
     
    /**
     * This function opens connect to the Shipwire database in MySQL. Creates the Shipwire database in MySQL
     *   if it does not exist
     * @return boolean.  True is returned if connection is made.
     */
     public function openConnection()
     {
         $this->dbLink = mysql_connect($this->server, $this->user, $this->password, true);         
         if (!$this->dbLink)  {
             return false;
         }
         
         $result = mysql_select_db($this->dbName, $this->dbLink);
         // If Shipwire database does not exist, then initialize databaase with Shipwire database and table.
         if (! $result)
         {
             $result = $this->initializeDb();
             if (! $result) {
                 return false;
             }            
         }
         return $this->dbLink;        
     }
     
     
    /**
     * This function closes the connection to the MySQL database.
     */
     public function closeConnection()
     {
         mysql_close($this->dbLink);
     }

     
    /**
     * This function performs queries on the Shipwire database in MySQL.
     * @param $sql.  A string for the mysql statement to perform.
     * @return boolean.  For INSERTs AND UPDATEs, true is returned if successful.  For SELECTS,
     *   rows of records are returned.
     */
     public function query($sql){
         $result = mysql_query($sql, $this->dbLink);

         if (! $result)  {
             return false;
         }
         if(stristr($sql, "SELECT"))   {
             $record = null;
             while ($row = mysql_fetch_assoc($result)) {
                 $record[] = $row;    
             }
             mysql_free_result($result);                 
             return $record;
         }
         return $result;
     }
 }
     
?>