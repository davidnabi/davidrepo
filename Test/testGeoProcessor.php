<?php
require_once "../GeoProcessor.inc";

print ("--- GeoProcessor Test ---\n\n");

$failedTestCount = 0;
$totalTestCount = 0;
 
$address1 = "435 Indio Way, Sunnyvale, CA 94085";  //Shipwire
$address2 = "North Pole";  // Northpole
$address3 = "Bennelong Point, Sydney NSW 2000, Australia";  // Sydney Opera House
$address4 = "Champ de Mars, 5 Avenue Anatole France, 75007 Paris, France";   // Eiffel Tower
$address5 = "350 5th Ave, New York, NY 10118";    //Empire State Building
$address6 = "Toyota, Aichi Prefecture, Japan";    //Toyota Headquarters
$address7 = "I do not exist so there is no way you can find me";   //No Way

$totalTestCount++;
$coordinates1 = GeoProcessor::getCoordinates($address1);
if (is_null($coordinates1))  {
    print ("coordinates1 failed\n");
    $failedTestCount++;
}

$totalTestCount++;
$coordinates2 = GeoProcessor::getCoordinates($address2);
if (is_null($coordinates2))  {
    print ("coordinates2 failed\n");
    $failedTestCount++;
}

$totalTestCount++;
$coordinates3 = GeoProcessor::getCoordinates($address3);
if (is_null($coordinates3))  {
    print ("coordinates3 failed\n");
    $failedTestCount++;
}

$totalTestCount++;
$coordinates4 = GeoProcessor::getCoordinates($address4);
if (is_null($coordinates4))  {
    print ("coordinates4 failed\n");
    $failedTestCount++;
}

$totalTestCount++;
$coordinates5 = GeoProcessor::getCoordinates($address5);
if (is_null($coordinates5))  {
    print ("coordinates5 failed\n");
    $failedTestCount++;
}

$totalTestCount++;
$coordinates6 = GeoProcessor::getCoordinates($address6);
if (is_null($coordinates6))  {
    print ("coordinates6 failed\n");
    $failedTestCount++;
}

$totalTestCount++;
$coordinates7 = GeoProcessor::getCoordinates($address7);
if (! (is_null($coordinates7)))  {
    print ("coordinates7 failed\n");
    $failedTestCount++;
}

$totalTestCount++;
$distance1And2 = GeoProcessor::getDistanceBetweenCoordinates($coordinates1["latitude"], $coordinates1["longitude"], $coordinates2["latitude"], $coordinates2["longitude"]);
if ($distance1And2 == 0)  {
    print ("Distance between coordinates 1 and 2 failed\n");
    $failedTestCount++;
}

$totalTestCount++;
$distance3And4 = GeoProcessor::getDistanceBetweenCoordinates($coordinates3["latitude"], $coordinates3["longitude"], $coordinates4["latitude"], $coordinates4["longitude"]);
if ($distance3And4 == 0)  {
    print ("Distance between coordinates 3 and 4 failed\n");
    $failedTestCount++;
}

$totalTestCount++;
$distance5And6 = GeoProcessor::getDistanceBetweenCoordinates($coordinates5["latitude"], $coordinates5["longitude"], $coordinates6["latitude"], $coordinates6["longitude"]);
if ($distance5And6 == 0)  {
    print ("Distance between coordinates 5 and 6 failed\n");
    $failedTestCount++;
}

$totalTestCount++;
$distance1And2 = GeoProcessor::getDistanceBetweenCoordinates($coordinates1["latitude"], $coordinates1["longitude"], $coordinates1["latitude"], $coordinates1["longitude"]);
if ($distance1And2 != 0)  {
    print ("Distance between coordinates 1 and 1 failed\n");
    $failedTestCount++;
}

if ($failedTestCount > 0)  {
    print ("$failedTestCount out of $totalTestCount failed\n");
}  else  {
    print ("All $totalTestCount test passed\n");
}

?>
