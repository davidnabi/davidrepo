<?php
require_once "../Warehouse.inc";
require_once "../DBHelper.inc";

// Create Database
$db = new DBHelper();
$db->openConnection();

$warehouse = new Warehouse($db);

print ("--- Warehouse Test ---\n\n");

$failedTestCount = 0;
$totalTestCount = 0;

$totalTestCount++;
$warehouseList = array();
$warehouseList[] = array("WA-US", "Seattle, Washington, US");
$warehouseList[] = array("NY-US", "Albany, New York, US");
$warehouseList[] = array("LDN-UK", "London, United Kingdom");
$warehouseList[] = array("SY-AU", "Sydney, AU");
$warehouseList[] = array("HK-CN", "Hong Kong, CN");
$result = $warehouse->create($warehouseList);
if ("OK" != $result["status"])  {
    print ("create 1 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$record = $warehouse->getWarehouseListAndCoordinates();
$sql = "SELECT name FROM " . Settings::$TABLE_WH;
$sqlResults = $db->query($sql);
if (count($record) != count( $sqlResults)) {
    print ("getWarehouseList 1 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$warehouseList = array(); 
$warehouseList[] = array("WA-US", "Seattle, Washington, US");
$warehouseList[] = array("JRS-IL", "Jerusalem, Israel");
$result = $warehouse->create($warehouseList);
$record = $warehouse->getWarehouseListAndCoordinates();
if ("WARNING" != $result["status"] || count($record)!=6)  {
    print ("create 2 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$record = $warehouse->getWarehouseListAndCoordinates();
$sql = "SELECT name FROM " . Settings::$TABLE_WH;
$sqlResults = $db->query($sql);
if (count($record) != count( $sqlResults)) {
    print ("getWarehouseList 2 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$warehouseList = array(); 
$warehouseList[] = array("NO-WHERE", "I DO NOT EXIST SO I CANNOT BE CREATED");
$result = $warehouse->create($warehouseList);
$record = $warehouse->getWarehouseListAndCoordinates();
if ("WARNING" != $result["status"] || count($record)!=6)  {
    print ("create 3 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$stockWarehouseList = array();
$stockWarehouseList[] = array("40 Inch TV", "50", "WA-US");
$stockWarehouseList[] = array("50 Inch TV", "50", "WA-US");
$stockWarehouseList[] = array("60 Inch TV", "50", "WA-US");
$stockWarehouseList[] = array("40 Inch TV", "10", "NY-US");
$stockWarehouseList[] = array("50 Inch TV", "10", "NY-US");
$stockWarehouseList[] = array("60 Inch TV", "10", "NY-US");
$result = $warehouse->addStock($stockWarehouseList);
$sql = "SELECT product FROM " . Settings::$TABLE_WH_INV;
$sqlResults = $db->query($sql);
if ("OK" != $result["status"] || count($sqlResults) !=6)  {
    print ("addStock 1 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$stockWarehouseList = array();
$stockWarehouseList[] = array("40 Inch TV", "30", "CA-US");
$stockWarehouseList[] = array("50 Inch TV", "30", "CA-US");
$stockWarehouseList[] = array("60 Inch TV", "30", "CA-US");
$result = $warehouse->addStock($stockWarehouseList);
$sql = "SELECT product FROM " . Settings::$TABLE_WH_INV;
$sqlResults = $db->query($sql);
if ("WARNING" != $result["status"] || count($sqlResults) !=6)  {
    print ("addStock 2 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$stockWarehouseList = array();
$stockWarehouseList[] = array("40 Inch TV", "10", "SY-AU");
$stockWarehouseList[] = array("50 Inch TV", "20", "SY-AU");
$stockWarehouseList[] = array("60 Inch TV", "30", "SY-AU");
$result = $warehouse->addStock($stockWarehouseList);
$sql = "SELECT product FROM " . Settings::$TABLE_WH_INV;
$sqlResults = $db->query($sql);
if ("OK" != $result["status"] || count($sqlResults) !=9)  {
    print ("addStock 3 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$stockWarehouseList = array();
$stockWarehouseList[] = array("40 Inch TV", "5", "WA-US");
$stockWarehouseList[] = array("50 Inch TV", "5", "NY-US");
$stockWarehouseList[] = array("60 Inch TV", "5", "SY-AU");
$result = $warehouse->addStock($stockWarehouseList);
$sql = "SELECT product FROM " . Settings::$TABLE_WH_INV;
$sqlResults = $db->query($sql);
if ("OK" != $result["status"] || count($sqlResults) !=9)  {
    print ("addStock 4 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$warehouseName =  "WA-US";
$orderList = array();
$orderList[] = array("40 Inch TV", "15");
$orderList[] = array("50 Inch TV", "15");
$orderList[] = array("60 Inch TV", "15");
$canFulfillOrder = $warehouse->doesWarehouseFulfillOrder($warehouseName, $orderList);
if (! $canFulfillOrder)  {
    print ("doesWarehouseFulfillOrder 1 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$warehouseName =  "SY-AU";
$orderList = array();
$orderList[] = array("40 Inch TV", "10");
$orderList[] = array("50 Inch TV", "20");
$orderList[] = array("60 Inch TV", "50");
$canFulfillOrder = $warehouse->doesWarehouseFulfillOrder($warehouseName, $orderList);
if ($canFulfillOrder)  {
    print ("doesWarehouseFulfillOrder 2 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$warehouseName =  "WA-US";
$orderList = array();
$orderList[] = array("40 Inch TV", "15");
$orderList[] = array("50 Inch TV", "15");
$orderList[] = array("60 Inch TV", "15");
$canUpdateInventory = $warehouse->updateInventoryAfterOrder($warehouseName, $orderList);
$sql = "SELECT quantity FROM " . Settings::$TABLE_WH_INV . " WHERE warehouse='$warehouseName' AND product = '50 Inch TV'";
$sqlResults = $db->query($sql);
if (! $canUpdateInventory || $sqlResults[0]["quantity"] != 35)  {
    print ("updateInventoryAfterOrder 1 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$warehouseName =  "NY-US";
$orderList = array();
$orderList[] = array("40 Inch TV", "10");
$orderList[] = array("50 Inch TV", "5");
$canUpdateInventory = $warehouse->updateInventoryAfterOrder($warehouseName, $orderList);
$sql = "SELECT quantity FROM " . Settings::$TABLE_WH_INV . " WHERE warehouse='$warehouseName' AND product = '40 Inch TV'";
$sqlResults = $db->query($sql);
if (! $canUpdateInventory || $sqlResults[0]["quantity"] != 0)  {
    print ("updateInventoryAfterOrder 1 failed\n");
    $failedTestCount++;    
}

// Drop Database
$sql = "DROP DATABASE " . Settings::$DB_NAME;
$db->query($sql);
$db->closeConnection();

if ($failedTestCount > 0)  {
    print ("$failedTestCount out of $totalTestCount failed\n");
}  else  {
    print ("All $totalTestCount test passed\n");
}
?>
