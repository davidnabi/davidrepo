<?php
require_once "../Product.inc";
require_once "../DBHelper.inc";

// Create Database
$db = new DBHelper();
$db->openConnection();

$product = new Product($db);

print ("--- Product Test ---\n\n");

$failedTestCount = 0;
$totalTestCount = 0;

$totalTestCount++;
$productList = array();
$productList[]= array("Samsung 50 INCH LED TV", "60IN X 30IN X 6IN", "40LB");
$productList[]= array("Maytag Refrigerator", "40IN X 80IN X 40IN", "300LB");
$productList[]= array("Dell Computer", "9IN X 20IN X 30IN", "15LB");
$productList[]= array("Webster Dictionary", "10IN X 8IN X 6IN", "15OZ");
$productList[]= array("Hon Metal Filing Cabinet", "15IN X 24IN X 30IN", "40LB");
$productList[]= array("Spalding Basketball", "9.5IN", "20OZ");
$result = $product->create($productList);
$sql = "SELECT name FROM " . Settings::$TABLE_PROD;
$sqlResults = $db->query($sql);
if ("OK" != $result["status"] || count($sqlResults) != 6)  {
    print ("create 1 failed\n");
    $failedTestCount++;    
}


$totalTestCount++;
$productList = array();
$productList[]= array("Rawling Baseballs", "12IN X 6IN X 12IN", "45OZ");
$productList[]= array("Dell Computer", "9IN X 20IN X 30IN", "15LB");
$productList[]= array("Swingline Paper Shredder", "10IN X 14IN X 20IN", "11LB");
$productList[]= array("Webster Dictionary", "10IN X 8IN X 6IN", "15OZ");
$productList[]= array("Rawling Baseballs", "12IN X 6IN X 12IN", "45OZ");
$result = $product->create($productList);
$sql = "SELECT name FROM " . Settings::$TABLE_PROD;
$sqlResults = $db->query($sql);
if ("WARNING" != $result["status"] || count($sqlResults) != 8)  {
    print ("create 2 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$productName = "Rawling Baseballs";
$doesProductExist = $product->doesProductExist($productName);
if (! $doesProductExist)  {
    print ("doesProductExist 1 failed\n");
    $failedTestCount++;    
}

$totalTestCount++;
$productName = "Beat Headphones";
$doesProductExist = $product->doesProductExist($productName);
if ($doesProductExist)  {
    print ("doesProductExist 2 failed\n");
    $failedTestCount++;    
}  
// Drop Database
$sql = "DROP DATABASE " . Settings::$DB_NAME;
$db->query($sql);
$db->closeConnection();

if ($failedTestCount > 0)  {
    print ("$failedTestCount out of $totalTestCount failed\n");
}  else  {
    print ("All $totalTestCount test passed\n");
}
?>
