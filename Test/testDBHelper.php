<?php
require_once "../DBHelper.inc";

$db = new DBHelper();  // Use internal settings to access MySQL

print ("--- DBHelper Test ---\n\n");

$failedTestCount = 0;
$totalTestCount = 0;

$totalTestCount++;
$canConnect = $db->openConnection();   // This will create Shipwire Database with tables Warehouse, WarehouseInventory, and Product.
if (! $canConnect)  {
    print ("openConnection 1 failed\n");
    $failedTestCount++;    
}    

$totalTestCount++;
$query = "SELECT count(*) FROM Warehouse";
$sqlResult = $db->query($query);
if (is_null($sqlResult) || $sqlResult[0]["count(*)"] != 0)  {
    print ("query 1 SELECT test failed\n");
    $failedTestCount++;    
}    

$totalTestCount++;
$query = "SELECT count(*) FROM NonExistingTable";
$sqlResult = $db->query($query);
if (is_null($sqlResult) || $sqlResult)  {
    print ("query 2 SELECT test failed\n");
    $failedTestCount++;    
}    

$totalTestCount++;
$query = "SELECT NonExistingColumn FROM Warehouse";
$sqlResult = $db->query($query);
if (is_null($sqlResult) || $sqlResult)  {
    print ("query 3 SELECT test failed\n");
    $failedTestCount++;    
}    

$totalTestCount++;
$query = "INSERT INTO Warehouse (name, address, longitude, latitude) VALUES ('MP-PU', 'machu picchu peru', -72.544963, -13.163141)";
$sqlResult = $db->query($query);
if (is_null($sqlResult) || ! $sqlResult)  {
    print ("query 4 INSERT test failed\n");
    $failedTestCount++;    
}    

$totalTestCount++;
$query = "INSERT INTO Warehouse (name) VALUES ('MU-PU')";
$sqlResult = $db->query($query);
if (is_null($sqlResult) || $sqlResult)  {
    print ("query 5 INSERT test failed\n");
    $failedTestCount++;    
}    

$totalTestCount++;
$query = "UPDATE Warehouse SET name= 'MFT-PE' WHERE address='machu picchu peru'";
$sqlResult = $db->query($query);
if (is_null($sqlResult) || ! $sqlResult)  {
    print ("query 6 UPDATE test failed\n");
    $failedTestCount++;    
}    
  
$totalTestCount++;
$query = "UPDATE Warehouse SET name= NULL WHERE address='machu picchu peru'";
$sqlResult = $db->query($query);
if (is_null($sqlResult) || $sqlResult)  {
    print ("query 7 UPDATE test failed\n");
    $failedTestCount++;    
}    
  
// Drop Database
$sql = "DROP DATABASE " . Settings::$DB_NAME;
$db->query($sql);
$db->closeConnection();

if ($failedTestCount > 0)  {
    print ("$failedTestCount out of $totalTestCount failed\n");
}  else  {
    print ("All $totalTestCount test passed\n");
}
?>
