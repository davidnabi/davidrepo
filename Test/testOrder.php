<?php
require_once "../Warehouse.inc";
require_once "../Product.inc";
require_once "../DBHelper.inc";
require_once "../Order.inc";
                              
function createProducts($product)  {
    $productList = array();
    $productList[]= array("Nike T-Shirt", "8IN X 12IN X 2IN", "15OZ");
    $productList[]= array("Nike Shorts", "8IN X 12IN X 2IN", "9OZ");
    $productList[]= array("Nike Socks", "8IN X 8IN X 2IN", "8OZ");
    $productList[]= array("Nike Cap", "10IN X 10IN X 6IN", "8OZ");
    $productList[]= array("Nike Court Shoes", "15IN X 24IN X 30IN", "20OZ");
    $productList[]= array("Nike Running Shoes", "15IN X 24IN X 30IN", "20OZ");
    /*
    $productList[]= array("Under Armor T-Shirt", "8IN X 12IN X 2IN", "15OZ");
    $productList[]= array("Under Armor Shorts", "8IN X 12IN X 2IN", "9OZ");
    $productList[]= array("Under Armor Socks", "8IN X 8IN X 2IN", "8OZ");
    $productList[]= array("Under Armor Cap", "10IN X 10IN X 6IN", "8OZ");
    $productList[]= array("Under Armor Court Shoes", "15IN X 24IN X 30IN", "20OZ");
    $productList[]= array("Under Armor Running Shoes", "15IN X 24IN X 30IN", "20OZ");
    $productList[]= array("Adidas T-Shirt", "8IN X 12IN X 2IN", "15OZ");
    $productList[]= array("Adidas Shorts", "8IN X 12IN X 2IN", "9OZ");
    $productList[]= array("Adidas Socks", "8IN X 8IN X 2IN", "8OZ");
    $productList[]= array("Adidas Cap", "10IN X 10IN X 6IN", "8OZ");
    $productList[]= array("Adidas Court Shoes", "15IN X 24IN X 30IN", "20OZ");
    $productList[]= array("Adidas Running Shoes", "15IN X 24IN X 30IN", "20OZ");
    */
    $result = $product->create($productList);   
}

function createWarehouse($warehouse)  {
    $warehouseList = array();
    $warehouseList[] = array("WA-US", "Seattle, Washington, United States");
    $warehouseList[] = array("NY-US", "Albany, New York, United States");
    $warehouseList[] = array("LDN-UK", "London, United Kingdom");
    $warehouseList[] = array("SY-AU", "Sydney, Australia");
    $warehouseList[] = array("HK-CN", "Hong Kong, China");
    $result = $warehouse->create($warehouseList);    
}

function stockWarehouse($warehouse)  {
    $stockWarehouseList = array();
    $stockWarehouseList[] = array("Nike T-Shirt", "10", "WA-US");
    $stockWarehouseList[] = array("Nike Shorts", "10", "WA-US");
    $stockWarehouseList[] = array("Nike Socks", "10", "WA-US");
    $stockWarehouseList[] = array("Nike Cap", "10", "WA-US");
    $stockWarehouseList[] = array("Nike Court Shoes", "10", "WA-US");
    $stockWarehouseList[] = array("Nike Running Shoes", "10", "WA-US");
    $stockWarehouseList[] = array("Nike T-Shirt", "50", "NY-US");
    $stockWarehouseList[] = array("Nike Shorts", "50", "NY-US");
    $stockWarehouseList[] = array("Nike Socks", "50", "NY-US");
    $stockWarehouseList[] = array("Nike Cap", "25", "NY-US");
    $stockWarehouseList[] = array("Nike Court Shoes", "25", "NY-US");
    $stockWarehouseList[] = array("Nike Running Shoes", "25", "NY-US");
    $stockWarehouseList[] = array("Nike T-Shirt", "30", "LDN-UK");
    $stockWarehouseList[] = array("Nike Shorts", "30", "LDN-UK");
    $stockWarehouseList[] = array("Nike Socks", "30", "LDN-UK");
    $stockWarehouseList[] = array("Nike Cap", "30", "LDN-UK");
    $stockWarehouseList[] = array("Nike Court Shoes", "15", "LDN-UK");
    $stockWarehouseList[] = array("Nike Running Shoes", "15", "LDN-UK");
    $stockWarehouseList[] = array("Nike T-Shirt", "40", "SY-AU");
    $stockWarehouseList[] = array("Nike Shorts", "40", "SY-AU");
    $stockWarehouseList[] = array("Nike Socks", "40", "SY-AU");
    $stockWarehouseList[] = array("Nike Cap", "40", "SY-AU");
    $stockWarehouseList[] = array("Nike Court Shoes", "40", "SY-AU");
    $stockWarehouseList[] = array("Nike Running Shoes", "60", "SY-AU");
    $stockWarehouseList[] = array("Nike T-Shirt", "100", "HK-CN");
    $stockWarehouseList[] = array("Nike Shorts", "100", "HK-CN");
    $stockWarehouseList[] = array("Nike Socks", "100", "HK-CN");
    $stockWarehouseList[] = array("Nike Cap", "100", "HK-CN");
    $stockWarehouseList[] = array("Nike Court Shoes", "10", "HK-CN");
    $stockWarehouseList[] = array("Nike Running Shoes", "10", "HK-CN");
    $warehouse->addStock($stockWarehouseList);
}

// Create Database
$db = new DBHelper();
$db->openConnection();

// Create products, warehouses, and stock warehouses.
$warehouse = new Warehouse($db);
$product = new Product($db);
createProducts($product);
createWarehouse($warehouse);
stockWarehouse($warehouse);

print ("--- Order Test ---\n\n");

$failedTestCount = 0;
$totalTestCount = 0;

$totalTestCount++;
$orderList = array();
$orderList[] = array("Nike T-Shirt", "15");
$orderList[] = array("Nike Shorts", "6");
$orderList[] = array("Nike Socks", "8");
$shipToAddress = "24 Willie Mays Plaza, San Francisco, CA 94107";  // AT&T Park
$results = Order::processOrder($orderList, $shipToAddress, $warehouse);
if ("OK" != $results["status"] || strpos($results["message"], "NY-US") === false)  {
    print ("processOrder 1 failed\n");
    $failedTestCount++;        
}

$totalTestCount++;
$orderList = array();
$orderList[] = array("Nike T-Shirt", "20");
$orderList[] = array("Nike Cap", "20");
$orderList[] = array("Nike Running Shoes", "30");
$shipToAddress = "Champ de Mars, 5 Avenue Anatole France, 75007 Paris, France";   // Eiffel Tower
$results = Order::processOrder($orderList, $shipToAddress, $warehouse);
if ("OK" != $results["status"] || strpos($results["message"], "SY-AU") === false)  {
    print ("processOrder 2 failed\n");
    $failedTestCount++;        
}

$totalTestCount++;
$orderList = array();
$orderList[] = array("Nike T-Shirt", "10");
$orderList[] = array("Nike Short", "10");
$orderList[] = array("Nike Court Shoes", "10");
$shipToAddress = "I do not exist.  I will never get it";   // No where on earth
$results = Order::processOrder($orderList, $shipToAddress, $warehouse);
if ("WARNING" != $results["status"])  {
    print ("processOrder 3 failed\n");
    $failedTestCount++;        
}
  
$totalTestCount++;
$orderList = array();
$orderList[] = array("Nike T-Shirt", "90");
$orderList[] = array("Nike Short", "90");
$orderList[] = array("Nike Court Shoes", "90");
$shipToAddress = "machu picchu peru";
$results = Order::processOrder($orderList, $shipToAddress, $warehouse);
if ("WARNING" != $results["status"])  {
    print ("processOrder 4 failed\n");
    $failedTestCount++;        
}

// Drop Database
$sql = "DROP DATABASE " . Settings::$DB_NAME;
$db->query($sql);
$db->closeConnection();

if ($failedTestCount > 0)  {
    print ("$failedTestCount out of $totalTestCount failed\n");
}  else  {
    print ("All $totalTestCount test passed\n");
}
?>
