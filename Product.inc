<?php
require_once "DBHelper.inc";

class Product  {
    private $db;
        
    public function Product($db){
        $this->db = $db;
    }

    /**
     * This function creates all the products.
     * @param $productList.  The array of products which includes name, dimensions, and weight.
     * @return array.  Array with status and message for creating the warehouse
     */
    public function create($productList) {
        $result = array("status" => "OK", "message" => "");

        foreach ($productList as $value)  {
            $name = $value[0];
            $dimensions = $value[1];
            $weight = $value[2];
            if ($this->doesProductExist($name))  {
                $result["status"] = "WARNING";
                $result["message"] .= "$name has already been created; ";
                continue;
            }
            
            $sql = "INSERT INTO " . Settings::$TABLE_PROD . " (name, dimensions, weight) VALUES('$name', '$dimensions', '$weight')";
            $sqlResult = $this->db->query($sql);
            if (! $sqlResult)  {
                $result["status"] = "WARNING";
                $result["message"] .= "$name has not been created - cannot store product; ";
            }
        }

        return $result;
        
    }

    /**
     * This function determines if product has already been created
     * @param $productName.  The name of the product.
     * @return boolean. True if product has already been created.
     */
    public function doesProductExist($productName) {
        $sql = "SELECT id, name FROM " . Settings::$TABLE_PROD . " WHERE name='" . $productName . "'";
        $sqlResult = $this->db->query($sql);
        if (is_null($sqlResult))  {
            return null;
        }
        if (count($sqlResult) == 0)  {
            return false;
        }
        return true;
    }
}
        
?>