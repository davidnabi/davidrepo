<?php
require_once "DBHelper.inc";
require_once "Warehouse.inc";
require_once "Product.inc";
require_once "Order.inc";
require_once "GeoProcessor.inc";

    /**
     * This function displays the menu console and ask user for operation.
     * @return String.  Selection for operation.
     */
    function displayAndGetSelection()  {
        while (true)  {
            // Display Menu on Console
            print ("Welcome to Shipwire Console.  Here is the menu of options.\n");
            print ("1. Create (W)arehouse locations.\n");
            print ("2. Create (P)roduct.\n");
            print ("3. (A)ssign product stock to warehouse.\n");
            print ("4. Create an (O)rder.\n");
            print ("5. E(X)it.\n");
            print ("Please select an operation to perform: ");
            $selection = trim(fgets(STDIN));
            
            if (("W" == $selection) || ("P" == $selection) || ("A" == $selection) || ("O" == $selection) || ("X" == $selection)) {
                break;
            } else {
                print ("\n$selection is an invalid selection.  Please try again.\n\n");
            }
        }
        return $selection;               
     }
 
    /**
     * This function ask user for warehouse location information.
     * @return Array.  Array of warehouse location with name and address.
     */
     function getWarehouseLocationInformation()  {
        print ("\n\n--- Create Warehouse Location ---\n");
        $warehouseLocationList = array();
        while (true)  {
            print ("Enter Warehouse Name: ");
            $name = trim(fgets(STDIN));
            print ("Enter Warehouse Address: ");
            $address = trim(fgets(STDIN));
            $warehouseLocationList[]=array($name, $address);
            print ("Anymore Warehouse to create (y/n): ");
            $anymore = trim(fgets(STDIN));
            if ("n" == $anymore)  {
                break;
            }
            print ("\n");
        }
        return $warehouseLocationList;               
     }
    
    /**
     * This function ask user for product information.
     * @return Array.  Array of product with name, dimensions, and weight.
     */
     function getProductInformation()  {
        print ("\n\n--- Create Product ---\n");
        $productInformationList = array();
        while (true)  {
            print ("Enter Product Name: ");
            $name = trim(fgets(STDIN));
            print ("Enter Product Dimensions: ");
            $dimensions = trim(fgets(STDIN));
            print ("Enter Product Weight: ");
            $weight = trim(fgets(STDIN));
            $productInformationList[] = array($name, $dimensions, $weight);
            print ("Anymore Product to create (y/n): ");
            $anymore = trim(fgets(STDIN));
            if ("n" == $anymore)  {
                break;
            }
            print ("\n");
        }
        return $productInformationList;               
    } 
    
     /**
     * This function ask user for products to stock at the warehouse.
     * @return Array.  Array of product stock information with name, quantity, and warehouse name.
     */
    function getStockWarehouseInformation()  {
        print ("\n\n--- Stock Warehouse ---\n");
        $stockWarehouseList = array();
        while (true)  {
            print ("Enter Product Name: ");
            $name = trim(fgets(STDIN));
            print ("Enter Product Quantity: ");
            $quantity = trim(fgets(STDIN));
            print ("Enter Warehouse Name: ");
            $warehouseName = trim(fgets(STDIN));
            $stockWarehouseList[] = array($name, $quantity, $warehouseName);
            print ("Anymore Products to Stock (y/n): ");
            $anymore = trim(fgets(STDIN));
            if ("n" == $anymore)  {
                break;
            }
            print ("\n");
        }
        return $stockWarehouseList;               
    } 

     /**
     * This function ask user for order information.
     * @return Array.  Array of product information with name and quantity.
     */
     function getOrderInformation()  {
        print ("\n\n--- Create Order ---\n");
        $orderList = array();
        while (true)  {
            print ("Enter Product Name: ");
            $name = trim(fgets(STDIN));
            print ("Enter Product Quantity: ");
            $quantity = trim(fgets(STDIN));
            $orderList[] = array($name, $quantity);
            print ("Anymore Products to Order (y/n): ");
            $anymore = trim(fgets(STDIN));
            if ("n" == $anymore)  {
                break;
            }
            print ("\n");
        }
        return $orderList;               
    } 

    $db = new DBHelper();
    $db->openConnection();
    $warehouse = new Warehouse($db);
    $product = new Product($db);
    
    while (true)  {
        $selection = displayAndGetSelection();
        if  ("X" == $selection) {
            print ("Thank you for using Shipwire console.\n\n\n");
            exit();
        }
        
        if ("W" == $selection)  {
            $warehouseLocationList = getWarehouseLocationInformation();
            $results = $warehouse->create($warehouseLocationList);
            if ("WARNING" == $results["status"])  {
                print ("WARNING: " . $results["message"]);
            }
        }
        
        if ("P" == $selection)  {
            $productInformationList = getProductInformation();
            $results = $product->create($productInformationList);
            if ("WARNING" == $results["status"])  {
                print ("WARNING: " . $results["message"]);
            }
        }

        if ("A" == $selection)  {
            $stockWarehouseList = getStockWarehouseInformation();

            // Check and remove product that do not exists.
            $stockMessage = "";
            $nonExistIndex = array();
            for  ($index=0; $index < count($stockWarehouseList); $index++)  {
                $name = $stockWarehouseList[$index][0];
                if (! $product->doesProductExist($name))  {
                    $stockMessage .= "$name does not exist; ";
                    $nonExistIndex[] = $index;
                }
            }
            foreach ($nonExistIndex as $index)  {
                unset($stockWarehouseList[$index]);
            }
            
            $results = $warehouse->addStock($stockWarehouseList);
            if ("WARNING" == $results["status"])  {
                $stockMessage .= $results["message"];
            }
            
            if (strlen($stockMessage) > 0)  {
                print ("Not all stock was assigned: $stockMessage");
                print ("\n\n");
            }
        }

        if ("O" == $selection)  {
            $orderList = getOrderInformation();
            print ("Enter Address to Ship Order: ");
            $shipToAddr = trim(fgets(STDIN));
            
            // Check if any of the products do not exists.
            $orderMessage = "";
            foreach ($orderList as $order)  {
                $name = $order[0];
                if (! $product->doesProductExist($name))  {
                    $orderMessage .= "$name does not exist; ";
                }
            }
        
            // Check if the ship to address is valid.
            $coordinates = GeoProcessor::getCoordinates($shipToAddr);
            if (is_null($coordinates)) {
                $orderMessage .= "cannot get coordinates for $shipToAddr; ";
            }
            
            // If check fails, display message
            if (strlen($orderMessage) > 0)  {
                print ("Cannot complete the order: $orderMessage");
                print ("\n\n");
                continue;    
            }
            
            $results = Order::processOrder($orderList, $shipToAddr, $warehouse); 
            if ("WARNING" == $results["status"])  {
                print ("WARNING: " . $results["message"]);
            }
            if ("OK" == $results["status"])  {
                print ($results["message"]);
            }
            
        }
        
        print ("\n\n");
    }
?>
