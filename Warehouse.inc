<?php
require_once "Settings.inc";
require_once "DBHelper.inc";
require_once "GeoProcessor.inc";

class Warehouse  {
    private $db;
        
    public function Warehouse($db){
        $this->db = $db;
    }
    
    /**
     * This function creates warehouse location information in the data store.
     * @param $warehouseLocationList.  An array of warehouse locations which includes name and address.
     * @return array.  Array with status and message for creating the warehouse
     */
    public function create($warehouseLocationList)  {
        $result = array("status" => "OK", "message" => "");
        
        foreach ($warehouseLocationList as $value)  {
            $name = $value[0];
            $address = $value[1];
            if ($this->doesWarehouseExist($name))  {
                $result["status"] = "WARNING";
                $result["message"] .= "$name has already been created; ";
                continue;
            }
            $coordinates = GeoProcessor::getCoordinates($address);
            if (is_null($coordinates)) {
                $result["status"] = "WARNING";
                $result["message"] .= "$name has not been created - cannot get coordinates for address; ";
                continue;
            }
            $latitude = $coordinates["latitude"];
            $longitude = $coordinates["longitude"];
            
            $sql = "INSERT INTO " . Settings::$TABLE_WH . " (name, address, longitude, latitude) VALUES ('$name', '$address', $longitude, $latitude)";
            $queryResult = $this->db->query($sql);
            if (! $queryResult)  {
                $result["status"] = "WARNING";
                $result["message"] .= "$name has not been created - cannot store warehouse; ";
            }
        }

        return $result;
    }
    
    /**
     * This function add stock to warehouses in the data store.
     * @param $stockWarehouseList.  An array of stock which includes name, quantity, and warehouse name.
     * @return array.  Array with status and message for adding stock to the warehouse
     */
    public function addStock($stockWarehouseList)  {
        $result = array("status" => "OK", "message" => "");
        
        foreach ($stockWarehouseList as $value)  {
            $product = $value[0];
            $quantity = $value[1];
            $warehouse = $value[2];
            if (! $this->doesWarehouseExist($warehouse))  {
                $result["status"] = "WARNING";
                $result["message"] .= "$warehouse does not exist - cannot add $product to stock; ";
                continue;
            }
            $existingQuantity = $this->getProductQuantityInWarehouse($product, $warehouse);
            if (is_null($existingQuantity))  {
                $sql = "INSERT INTO " . Settings::$TABLE_WH_INV . " VALUES('$warehouse', '$product', $quantity)";
                $queryResult = $this->db->query($sql);
                if (is_null($queryResult))  {
                    $result["status"] = "WARNING";
                    $result["message"] .= "Cannot add $product to stock - cannot store stock; ";
                    continue;
                }
            }  else  {
                $newQuantity = $existingQuantity + $quantity;
                $sql = "UPDATE " . Settings::$TABLE_WH_INV . " SET quantity=$newQuantity WHERE product='$product' AND warehouse='$warehouse'";
                $queryResult = $this->db->query($sql);
                if (is_null($queryResult))  {
                    $result["status"] = "WARNING";
                    $result["message"] .= "Cannot update quantity of $product in stock - cannot update stock; ";
                }
            }            
        }
        
        return $result;       
    }
    
    /**
     * This function gets the list of warehouses and its coordinates.
     * @return array.  Array with warehouse name, latitude, and longitude
     */
    public function getWarehouseListAndCoordinates()  {
        $sql = "SELECT name, latitude, longitude FROM " . Settings::$TABLE_WH;
        $queryResults = $this->db->query($sql);
        return $queryResults;
    }
    
    /**
     * This function determines if a warehouse has the inventory to fulfill an order listing.
     * @param $warehouseName.  The name of the warehouse to check inventory
     * @param $orderList.  The list of order with the product name and quantity.
     * @return boolean.  True if the warehouse can fulfill the order  
     */
    public function doesWarehouseFulfillOrder($warehouseName, $orderList)  {
        foreach ($orderList as $order)  {
            $product = $order[0];
            $quantity = $order[1];
            $sql = "SELECT quantity FROM " . Settings::$TABLE_WH_INV . " WHERE warehouse='$warehouseName' AND product='$product' AND quantity >=$quantity";
            $queryResults = $this->db->query($sql);
            if (is_null($queryResults))  {
                return false;
            }
        }
        return true;     
    }
        
    /**
     * This function updates the inventory of a warehouse after the order is fulfilled.
     * @param $warehouseName.  The name of the warehouse to check inventory
     * @param $orderList.  The list of order with the product name and quantity.
     * @return boolean.  True if the warehouse inventory is updated
     */
    public function updateInventoryAfterOrder($warehouseName, $orderList)  {
        foreach ($orderList as $order)  {
            $product = $order[0];
            $quantity = $order[1];
            $sql = "SELECT quantity FROM " . Settings::$TABLE_WH_INV . " WHERE warehouse='$warehouseName' AND product='$product'";
            $queryResults = $this->db->query($sql);
            if (is_null($queryResults))  {
                return false;
            }
            $queryQuantity = $queryResults[0]["quantity"];
            $newQuantity = $queryQuantity - $quantity;
            $sql = "UPDATE " . Settings::$TABLE_WH_INV . " SET quantity=$newQuantity WHERE warehouse='$warehouseName' AND product='$product'";
            $queryResults = $this->db->query($sql);
            if (is_null($queryResults))  {
                return false;
            }
        }
        return true;     
    }

    /**
     * This function determines if the warehouse exists
     * @param $warehouseName.  The name of the warehouse
     * @return boolean.  True if the warehouse exists
     */
    private function doesWarehouseExist($warehouseName)  {
        $sql = "SELECT id, name FROM " . Settings::$TABLE_WH . " WHERE name='$warehouseName'";
        $queryResults = $this->db->query($sql);
        if (is_null($queryResults))  {
            return null;
        }
        if (count($queryResults) == 0)  {
            return false;
        }
        return true;
    } 
              
    /**
     * This function gets the quantity of a product from the warehouse
     * @param $productName.  The name of the product
     * @param $warehouseName.  The name of the warehouse
     * @return integer.  The quantity of the product in the warehouse
     */
    private function getProductQuantityInWarehouse($productName, $warehouseName) {
        $sql = "SELECT quantity FROM " . Settings::$TABLE_WH_INV . " WHERE product='$productName' AND warehouse='$warehouseName'";
        $queryResults = $this->db->query($sql);
        if (is_null($queryResults))  {
            return null;
        }
        if (count($queryResults) == 0)  {
            return 0;
        }
        
        return intval($queryResults[0]["quantity"]);
    }
}
        
?>