<?php
require_once "Settings.inc";

class GeoProcessor  {
    private static $EARTH_RADIUS_MILE= 3961;
    private static $GOOGLE_GEOCODE_URL = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
 
        
    /**
     * This function obtains the coordinates (longitude and latitude) of the input address
     *   Google Maps Geocoding API
     * @param $address to which to find the coordinates.
     * @return an array with latitude and longitude of the coordinates if they can be obtained.  Otherwise
     *   null is returned.
     */
    public static function getCoordinates($address) {
        $coordinates = null;
        $modifiedAddress = str_replace(" ", "+", $address);
        $url = self::$GOOGLE_GEOCODE_URL . $modifiedAddress . '&key=' . Settings::$GOOGLE_API_KEY;
        $results = @file_get_contents($url);
        if (! $results)  {
            return $coordinates;
        }
        $resultMap=json_decode($results, true);
        if ("OK" != $resultMap["status"])  {
            return $coordinates;
        }
        $longitude = $resultMap["results"][0]["geometry"]["location"]["lng"];
        $latitude = $resultMap["results"][0]["geometry"]["location"]["lat"];
        $coordinates = array("longitude" => $longitude, "latitude" => $latitude);
        return $coordinates;
    }
    
    /**
     * This function uses the Haversine Formula to calculate the distance between 2 coordinates.
     * @param latitudes and longitudes of 2 coordinates.
     * @return the disance in miles between the 2 coordinates.
     */
    public static function getDistanceBetweenCoordinates($latitude1, $longitude1, $latitude2, $longitude2)  {
        $lat1 =  deg2rad($latitude1);
        $lon1 =  deg2rad($longitude1);
        $lat2 =  deg2rad($latitude2);
        $lon2 =  deg2rad($longitude2);
        $dlat = $lat2 - $lat1;
        $alpha = $dlat/2;
        $dlon = $lon2 - $lon1;
        $beta = $dlon/2;
        $a = sin($alpha) * sin($alpha) + cos($lat1) * cos($lat2) * sin($beta) * sin($beta);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a) );
        $distance = self::$EARTH_RADIUS_MILE * $c;
        return $distance;
    }    
}

?>